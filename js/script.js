let autobiography = {
    firstName:"Alina",
    lastName:"Bezkorovayna",
    family:{
        mum:"Viktoria",
        dad:"Oleksandr",
    },
};
function cloneObject(autobiography){
    let clone ={};

    for(let key in autobiography) {

        if (typeof autobiography[key] == "object") {
            clone[key] =cloneObject(autobiography[key]);
            }
        else
            {
                clone[key] = autobiography[key];
            }
        }
    return clone;
}
console.log (cloneObject(autobiography));